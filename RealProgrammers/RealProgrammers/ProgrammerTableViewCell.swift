//
//  ProgrammerTableViewCell.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import UIKit

class ProgrammerTableViewCell: UITableViewCell {
}

extension ProgrammerTableViewCell: ProgrammerCellView {
    func display(name: String) {
        self.textLabel?.text = name
    }
    
    func display(favorite: Bool) {
    }
    
    func display(interviewDate: String) {
    }
}
