//
//  ProgrammersListPresenter.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

class ProgrammersListPresenter {
    
    var numberOfProgrammers: Int {
        return self.programmers.count
    }
    let useCase: ShowProgrammersListUseCase
    
    fileprivate var programmers: [ProgrammerResponse] = []
    
    init(_ useCase: ShowProgrammersListUseCase) {
        self.useCase = useCase
    }
    
    func viewReady() {
        useCase.showProgrammers()
    }
    
    func configure(_ cell: ProgrammerCellView, forRow row: Int) {
        assert(row < self.programmers.count)
        cell.display(name: programmers[row].name)
        cell.display(interviewDate: programmers[row].interviewDate.relativeDescription())
    }
}

extension ProgrammersListPresenter: ProgrammersListPresentation {
    func display(programmers: [ProgrammerResponse]) {
        self.programmers = programmers
    }
}
