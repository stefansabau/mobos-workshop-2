//
//  Programmer.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

struct Programmer {
    var name: String
    var emacs: Int
    var caffeine: Int
    var realProgrammerRating: Int
    var interviewDate: Date
    var favorite: Bool
}
