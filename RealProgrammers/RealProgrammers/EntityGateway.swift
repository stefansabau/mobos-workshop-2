//
//  EntityGateway.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

protocol EntityGateway {
    func fetchProgrammers() -> [Programmer]
}


