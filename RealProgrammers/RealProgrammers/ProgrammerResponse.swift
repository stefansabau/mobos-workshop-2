//
//  ProgrammerResponse.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

struct ProgrammerResponse {
    var name: String
    var interviewDate: Date
    var favorite: Bool
    
    init(programmer: Programmer) {
        self.name = programmer.name
        self.interviewDate = programmer.interviewDate
        self.favorite = programmer.favorite
    }
}
