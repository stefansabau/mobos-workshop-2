//
//  ShowProgrammersListUseCase.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

class ShowProgrammersListUseCase {
    
    let entityGateway: EntityGateway
    var presenter: ProgrammersListPresentation!

    init(_ entityGateway: EntityGateway) {
        self.entityGateway = entityGateway
    }
    
    func showProgrammers() {
        // Grab data
        let programmers = entityGateway.fetchProgrammers()
        
        // Transform data into response
        let responses = programmers.map { ProgrammerResponse(programmer: $0) }
        
        // Tell the presenter the response
        presenter.display(programmers: responses)
    }
}

