//
//  ProgrammerCellView.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation

protocol ProgrammerCellView {
    func display(name: String)
    func display(favorite: Bool)
    func display(interviewDate: String)
}
