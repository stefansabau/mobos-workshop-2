//
//  InMemoryRepo.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import Foundation


class InMemoryRepo: EntityGateway {
    
    private let programmers = [Programmer(name: "Real Programmer", emacs: 1, caffeine: 2, realProgrammerRating: 3, interviewDate: Date(), favorite: true), Programmer(name: "Not so Real Programmer", emacs: 2, caffeine: 5, realProgrammerRating: 3, interviewDate: Date(), favorite: true), Programmer(name: "True Programmer", emacs: 3, caffeine: 1, realProgrammerRating: 5, interviewDate: Date(), favorite: true)]
    
    func fetchProgrammers() -> [Programmer] {
        return programmers
    }
}
