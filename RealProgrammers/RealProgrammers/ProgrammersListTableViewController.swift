//
//  ProgrammersListTableViewController.swift
//  RealProgrammers
//
//  Created by Stefan Sabau on 17/02/2017.
//  Copyright © 2017 Stefan. All rights reserved.
//

import UIKit

class ProgrammersListTableViewController: UITableViewController {
    
    var presenter: ProgrammersListPresenter!
    
    private let reuseIdentifier = "ProgrammersListCellReuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewReady()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfProgrammers
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        guard let programmerCell = cell as? ProgrammerTableViewCell else {
            assertionFailure()
            return cell
        }
        
        presenter.configure(programmerCell, forRow: indexPath.row)
        
        return cell
    }
}

